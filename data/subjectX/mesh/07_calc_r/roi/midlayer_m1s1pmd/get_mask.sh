export SUBJECTS_DIR='/data/pt_01756/studies/unit_tests/data/subjectX/mesh/07_calc_r'
cd /data/pt_01756/studies/unit_tests/data/subjectX/mesh/07_calc_r
rm -f fsaverage
rm -f m2m_subjectX/surf
ln -s /afs/cbs.mpg.de/software/freesurfer/7.3.2/debian-bullseye-amd64/subjects/fsaverage 'fsaverage'
ln -s 'surfaces' 'm2m_subjectX/surf'
mri_surf2surf --srcsubject 'fsaverage' --srcsurfval '/data/pt_01756/masks/lefthandknob_M1S1PMd.overlay' --trgsurfval /data/pt_01756/studies/unit_tests/data/subjectX/mesh/07_calc_r/roi/midlayer_m1s1pmd/mask_subjectXm1pmdss.mgh --hemi lh --trgsubject m2m_subjectX --trgsurfreg sphere.reg.gii --srcsurfreg sphere.reg
