import pynibs
import copy
import numpy as np

from inspect import getsourcefile
import os

# path of create_subject_XXXXX.py file (has to be in subjects root directoy
subject_folder = os.path.split(os.path.abspath(getsourcefile(lambda: 0)))[0]

# subject information
########################################################################################################################
subject_id = 'spheremodel'
fn_subject_obj = subject_folder + '/spheremodel.hdf5'

# mri information
########################################################################################################################
mri = [dict()]
mri[0]['fn_mri_T1'] = subject_folder + '/mri/0/T1_sphere.nii.gz'
mri[0]['fn_mri_T2'] = None
mri[0]['fn_mri_DTI'] = None
mri[0]['fn_mri_DTI_rev'] = None
mri[0]['fn_mri_DTI_bvec'] = None
mri[0]['fn_mri_DTI_bval'] = None
mri[0]['dti_readout_time'] = None
mri[0]['dti_phase_direction'] = None

# mesh information
########################################################################################################################
mesh = dict()

mesh_id = '02_make_mesh_from_mri_simnibs4'
mesh_dict = dict()
mesh_dict['info'] = 'charm --v 1.0 (simnibs 4.0 beta)'
mesh_dict['approach'] = 'charm'
mesh_dict['mesh_folder'] = os.path.join(subject_folder, 'mesh', mesh_id)
mesh_dict['vertex_density'] = None
mesh_dict['fn_mesh_msh'] = os.path.join(mesh_dict['mesh_folder'], f'm2m_{subject_id}', f'{subject_id}.msh')
mesh_dict['fn_mesh_hdf5'] = os.path.join(mesh_dict['mesh_folder'], f'm2m_{subject_id}', f'{subject_id}.hdf5')
mesh_dict['fn_tensor_vn'] = None
mesh_dict['mri_idx'] = 0
mesh_dict['fn_mri_conform'] = f"m2m_{subject_id}/T1.nii.gz"
mesh_dict['fn_lh_wm'] = None
mesh_dict['fn_rh_wm'] = None
mesh_dict['fn_lh_gm'] = None
mesh_dict['fn_rh_gm'] = None
mesh_dict['fn_lh_gm_curv'] = None
mesh_dict['fn_rh_gm_curv'] = None
mesh_dict['fn_lh_midlayer'] = f'm2m_{subject_id}/surfaces/lh.central.gii'
mesh_dict['fn_rh_midlayer'] = f'm2m_{subject_id}/surfaces/rh.central.gii'
mesh_dict['smooth_skin'] = .8
mesh[mesh_id] = mesh_dict

mesh_id = '03_refine_mesh'
mesh_dict = dict()
mesh_dict['info'] = 'charm --v 1.0 (simnibs 4.0 beta)'
mesh_dict['approach'] = 'charm'
mesh_dict['mesh_folder'] = os.path.join(subject_folder, 'mesh', mesh_id)
mesh_dict['vertex_density'] = None
mesh_dict['fn_mesh_msh'] = os.path.join(mesh_dict['mesh_folder'], f'm2m_{subject_id}', f'{subject_id}.msh')
mesh_dict['fn_mesh_hdf5'] = os.path.join(mesh_dict['mesh_folder'], f'm2m_{subject_id}', f'{subject_id}.hdf5')
mesh_dict['fn_tensor_vn'] = None
mesh_dict['mri_idx'] = 0
mesh_dict['fn_mri_conform'] = f"m2m_{subject_id}/T1.nii.gz"
mesh_dict['fn_lh_wm'] = None
mesh_dict['fn_rh_wm'] = None
mesh_dict['fn_lh_gm'] = None
mesh_dict['fn_rh_gm'] = None
mesh_dict['fn_lh_gm_curv'] = None
mesh_dict['fn_rh_gm_curv'] = None
mesh_dict['fn_lh_midlayer'] = f'm2m_{subject_id}/surfaces/lh.central.gii'
mesh_dict['fn_rh_midlayer'] = f'm2m_{subject_id}/surfaces/rh.central.gii'
mesh_dict['smooth_skin'] = .8
mesh_dict['refinement_roi'] = 'midlayer_m1s1pmd'
mesh_dict['refinemement_element_size'] = 1
mesh[mesh_id] = mesh_dict

# roi information (first index: mesh, second index: roi)
########################################################################################################################
roi = {}

# create the same roi for each mesh
for mesh_id in mesh.keys():
    roi[mesh_id] = {}
    roi[mesh_id]['midlayer_m1s1pmd'] = {}
    roi[mesh_id]['midlayer_m1s1pmd']['type'] = 'surface'
    roi[mesh_id]['midlayer_m1s1pmd']['info'] = 'freesurfer PMd, M1 and somatosensory cortex'
    roi[mesh_id]['midlayer_m1s1pmd']['gm_surf_fname'] = mesh[mesh_id]['fn_lh_gm']
    roi[mesh_id]['midlayer_m1s1pmd']['wm_surf_fname'] = mesh[mesh_id]['fn_lh_wm']
    roi[mesh_id]['midlayer_m1s1pmd']['midlayer_surf_fname'] = mesh[mesh_id]['fn_lh_midlayer']
    roi[mesh_id]['midlayer_m1s1pmd']['refine'] = False
    roi[mesh_id]['midlayer_m1s1pmd']['delta'] = 0.5
    roi[mesh_id]['midlayer_m1s1pmd']['X_ROI'] = None
    roi[mesh_id]['midlayer_m1s1pmd']['Y_ROI'] = None
    roi[mesh_id]['midlayer_m1s1pmd']['Z_ROI'] = None
    roi[mesh_id]['midlayer_m1s1pmd']['layer'] = 3
    roi[mesh_id]['midlayer_m1s1pmd']['fn_mask'] = f"roi/midlayer_m1s1pmd/mask_{subject_id}m1pmdss.mgh"
    roi[mesh_id]['midlayer_m1s1pmd']['fn_mask_avg'] = '/data/pt_01756/masks/lefthandknob_M1S1PMd.overlay'
    roi[mesh_id]['midlayer_m1s1pmd']['hemisphere'] = "lh"

# experiment information
########################################################################################################################
exp = dict()
exp["reg_isi_05"] = dict()
exp["reg_isi_05"]['info'] = ['TMS-MEP M1 Regression study (random sampling), ISI=5']
exp["reg_isi_05"]['date'] = ['09/04/2021']
exp["reg_isi_05"]['nnav_system'] = 'Localite'
exp["reg_isi_05"]['fn_tms_nav'] = [[
    f'{subject_folder}/exp/reg_isi_05/tms_navigator/Claudia_20210401_15484.08_68a4e7d6/Sessions'
    f'/Session_20210409134021231/TMSTrigger/TriggerMarkers_Coil1_20210409170817799.xml',
    '']]
exp["reg_isi_05"]['fn_data'] = [[f'{subject_folder}/exp/reg_isi_05/mep/tms_mapping_210409_000.mat']]
exp["reg_isi_05"]['fn_intensity'] = None
exp["reg_isi_05"]['mep_onsets'] = [[0]]
exp["reg_isi_05"]['fn_exp_csv'] = [f'{subject_folder}/exp/reg_isi_05/experiment.csv']
exp["reg_isi_05"]['fn_exp_hdf5'] = [f'{subject_folder}/exp/reg_isi_05/experiment.hdf5']
exp["reg_isi_05"]['fn_coil'] = [['/data/pt_01756/coils/Magventure MCF-B65/Medtronic_MCF_B65_REF.nii.gz']]
exp["reg_isi_05"]['fn_mri_nii'] = [[
    f'{subject_folder}/exp/reg_isi_05/tms_navigator/Claudia_20210401_15484.08_68a4e7d6/BinData/NIFTI'
    f'/15484.08_T1fs_conform.nii']]
exp["reg_isi_05"]['cond'] = [[""]]
exp["reg_isi_05"]['channels'] = ["FDI", "ADM", "APB"]
exp["reg_isi_05"]['tms_pulse_time'] = 0.3
exp["reg_isi_05"]['experimenter'] = 'Konstantin Weise'
exp["reg_isi_05"]['mep_fit_info'] = [""]
exp["reg_isi_05"]['incidents'] = []
exp["reg_isi_05"]['postproc'] = [dict()]
exp["reg_isi_05"]['postproc'][0]['info'] = 'None'
exp["reg_isi_05"]['postproc'][0]['cmd'] = ' '

# # add plot settings for c=1/dE of |E| (MEP vs E shift) 21/06/2018
# ######################################################################################################################
ps = None
# save subject information in hdf5 file
pynibs.save_subject(fname=fn_subject_obj,
                    mri_dict=mri,
                    mesh_dict=mesh,
                    roi_dict=roi,
                    exp_dict=exp,
                    ps_dict=ps,
                    subject_id=subject_id,
                    subject_folder=subject_folder,
                    verbose=False)

print(f'Created subject .hdf5 file: {fn_subject_obj}')
