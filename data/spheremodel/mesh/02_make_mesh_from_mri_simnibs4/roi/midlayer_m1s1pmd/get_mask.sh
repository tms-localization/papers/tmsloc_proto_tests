export SUBJECTS_DIR='/data/pt_01756/studies/unit_tests/data/spheremodel/mesh/02_make_mesh_from_mri_simnibs4'
cd /data/pt_01756/studies/unit_tests/data/spheremodel/mesh/02_make_mesh_from_mri_simnibs4
rm -f fsaverage
rm -f m2m_spheremodel/surf
ln -s /afs/cbs.mpg.de/software/freesurfer/7.3.2/debian-bullseye-amd64/subjects/fsaverage 'fsaverage'
ln -s 'surfaces' 'm2m_spheremodel/surf'
mri_surf2surf --srcsubject 'fsaverage' --srcsurfval '/data/pt_01756/masks/lefthandknob_M1S1PMd.overlay' --trgsurfval /data/pt_01756/studies/unit_tests/data/spheremodel/mesh/02_make_mesh_from_mri_simnibs4/roi/midlayer_m1s1pmd/mask_spheremodelm1pmdss.mgh --hemi lh --trgsubject m2m_spheremodel --trgsurfreg sphere.reg.gii --srcsurfreg sphere.reg
