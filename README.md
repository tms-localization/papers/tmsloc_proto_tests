# Tests for *Precise motor mapping with transcranial magnetic stimulation*

This repository holds tests and test data for our protocol paper *Precise motor mapping with transcranial magnetic stimulation* (doi: [10.1038/s41596-022-00776-6](https://doi.org/10.1038/s41596-022-00776-6)).

![](https://images.zapnito.com/uploads/IzJvDtT1R9eutku4jweo_graphical_abstract.png)

Each of the scripts (`01...py` - `09...py`) from the [tmsloc_proto](https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto) repository is tested to identify bugs in the code or problems with the local installation. Test data includes pre-computed head meshes and experimental data samples.  
SimNIBS 4 is used for all tests.

# How-to run the tests
1. Clone the scripts repository:

       git clone https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto
2. Install **SimNIBS**:  

       conda env update -f https://github.com/simnibs/simnibs/releases/download/v4.0.0/environment_linux.yml --name tms_loco
       conda activate tms_loco
       pip install https://github.com/simnibs/simnibs/releases/download/v4.0.0/simnibs-4.0.0-cp39-cp39-linux_x86_64.whl

3. Install latest **pyNIBS** release:       

        pip install pyNIBS
    or get the `dev` version from git:

        git clone https://gitlab.gwdg.de/tms-localization/pynibs
        cd pynibs
        git checkout dev
        pip install -r requirements.txt
        python setup.py develop
        cd ../tmsloc_protoc
        git checkout dev
        cd --

4. Make sure to follow additional software requirements as described in the [scripts repo](https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto).
5. Clone this repo to get the tests and the testdata:

        git clone https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto_tests 
6. Run tests:

        cd tmsloc_proto_tests
        pytest -s .

Running all tests will take about 30 minutes.
        
