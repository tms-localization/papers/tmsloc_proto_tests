"""
Tests the TMSLoc_Proto script merge_exp_data.py with mesh `05_merge_exp_data` from
the `subjectX` subject.
"""
import shutil
import unittest
import os
import pynibs
import h5py
import pandas as pd
from . import SCRIPTS_FOLDER, DATA_FOLDER, EXEC


class Test05MergeExpData(unittest.TestCase):
    # Set path to script repo
    exp_folder = None
    subject = None
    merge_script = os.path.join(SCRIPTS_FOLDER, '05_merge_exp_data.py')
    subject_id = 'subjectX'
    subject_fn = os.path.join(DATA_FOLDER, subject_id, f'{subject_id}.hdf5')
    mesh_id = '05_merge_exp_data'
    exp_id = 'reg_isi_05'

    @classmethod
    def setUpClass(cls):
        """
        Prepare some stuff, call prep_folders.py script to remove any old data that we don't want.
        """
        print("Preparing Test05MergeExpData.")
        cls.subject = pynibs.load_subject(cls.subject_fn)
        cls.mesh_folder = cls.subject.mesh[cls.mesh_id]['mesh_folder']
        cls.exp_folder = os.path.join(f'{cls.subject.subject_folder}', 'exp', f'{cls.exp_id}')

        # remove stuff from old runs
        try:
            shutil.rmtree(os.path.join(f"{cls.exp_folder}", "mesh_05_merge_exp_data"))
        except FileNotFoundError:
            pass
        fn = os.path.join(f"{cls.exp_folder}", "mep", "delta_t_mep_vs_tms.png")
        if os.path.exists(fn):
            os.unlink(fn)

        print("Test preparation done.")

    def test_01_merge_data(self):
        """
        Calls our 05_merge_data script

        """
        cmd = f"{EXEC} " \
              f"{self.merge_script} " \
              f"-s {self.subject_fn} " \
              f"-m {self.mesh_id} " \
              f"-e {self.exp_id} " \
              f"-o " \
              f"-d " \
              f"-r"
        print(f"Running {cmd}")
        assert os.system(cmd) == 0, f"Error running {cmd}"
        #  "mep/delta_t_mep_vs_tms.png",
        for fn in ["experiment.hdf5",
                   "matsimnibs.hdf5",
                   "plot_coil_pos.hdf5",
                   "plot_coil_pos.xdmf",
                   "distance_histogram_orig.png", "plot_coil_pos.hdf5",
                   "plot_coil_pos.xdmf",
                   "nnav2simnibs/T1_deface_RAS.nii.gz"]:
            assert os.path.exists(os.path.join(f"{self.exp_folder}/mesh_{self.mesh_id}/{fn}")), f"{fn} wasn't created."

    def test_02_load_exp(self):
        fn_matsimnibs = "matsimnibs.hdf5"
        fn_coilpos_hdf5 = os.path.join(f"{self.exp_folder}", f"mesh_{self.mesh_id}", fn_matsimnibs)
        fn_exp_hdf5 = os.path.join(f"{self.exp_folder}", f"mesh_{self.mesh_id}", "experiment.hdf5")
        with h5py.File(fn_coilpos_hdf5, 'r') as f:
            n_coils = f['/matsimnibs'].shape[2]
        assert n_coils == 8
        df_phys_data_postproc_emg = pd.read_hdf(fn_exp_hdf5,
                                                "phys_data/postproc/EMG")

        assert df_phys_data_postproc_emg.shape == (8, 10)
