"""
Tests the TMSLoc_Proto script 01_create_subject_structure.py and creates the subject objects used in the testsuite.
Must be called once after cloning the repo.

"""
import os
import unittest
import tempfile
from time import sleep
import pynibs
from . import SCRIPTS_FOLDER, DATA_FOLDER, EXEC
import pathlib


class Test01CreateSubjectStructure(unittest.TestCase):
    os.chdir(pathlib.Path(__file__).parent.resolve())
    # Set path to script repo
    create_subject_struct_fn = os.path.join(SCRIPTS_FOLDER, '01_create_subject_structure.py')

    # Write all data in a temporary folder
    test_folder = tempfile.TemporaryDirectory()
    subject_id = 'testsubject'
    mesh_idx = 'charm_4.1_refined_M1_fs'
    roi_id = 'midlayer_m1s1pmd'

    @classmethod
    def setUpClass(cls):
        pass

    def test_01_create_subject(self):
        """
        Call 01_create_subject_structure.py
        """
        cmd = f"{EXEC} {self.create_subject_struct_fn} " \
              f"-f {os.path.join(self.test_folder.name, self.subject_id)}"
        assert os.system(cmd) == 0
        sleep(5)  # let's wait a little to compute the subject.hdf5 file

    def test_02_load_subject(self):
        subject = pynibs.load_subject(os.path.join(self.test_folder.name, self.subject_id, f"{self.subject_id}.hdf5"))
        assert subject.id == self.subject_id
        assert list(subject.mesh.keys())[0] == self.mesh_idx
        assert subject.exp in [None, {}]
        assert self.roi_id in subject.roi[self.mesh_idx]

    def test_03_folder(self):
        subject_folder = os.path.join(self.test_folder.name, self.subject_id)
        for f in ['exp', 'mesh', 'mri', 'opt', 'results']:
            folder2test = os.path.join(subject_folder, f)
            assert os.path.exists(folder2test), f"{folder2test} has not been created."

    def test_04_create_real_subjects(self):
        """Create both subjects used in the testsuite. """
        for subid in ['spheremodel', 'subjectX']:
            subject_py_fn = os.path.join(DATA_FOLDER, subid, f"create_subject_{subid}.py")
            subject_hdf5_fn = os.path.join(DATA_FOLDER, subid, f"{subid}.hdf5")
            cmd = f"{EXEC} {subject_py_fn}"
            assert os.system(cmd) == 0, f"Running {cmd} failed."
            assert os.path.exists(subject_hdf5_fn), f"{subject_hdf5_fn} wasn't created."


if __name__ == '__main__':
    unittest.main()
