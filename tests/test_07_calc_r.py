"""
Tests the TMSLoc_Proto script 06_calc_e.py with mesh `07_calc_r` from the `subjectX` subject.
"""
import shutil
import unittest
import os
import pynibs
import h5py
import numpy as np
from . import SCRIPTS_FOLDER, DATA_FOLDER, EXEC


class Test07CalcR(unittest.TestCase):
    # Set path to script repo
    subject = None
    res_folder_e = None
    calc_r_script = os.path.join(SCRIPTS_FOLDER, '07_calc_r2.py')
    subject_id = 'subjectX'
    subject_fn = os.path.join(DATA_FOLDER, subject_id, f'{subject_id}.hdf5')
    mesh_id = '07_calc_r'
    exp_id = 'reg_isi_05'
    roi_id = 'midlayer_m1s1pmd'
    muscles = ['ADM', 'FDI', 'APB']

    @classmethod
    def setUpClass(cls):
        """
        Prepare some stuff, call prep_folders.py script to remove any old data that we don't want.
        """
        print("Preparing Test07CalcR.")
        cls.subject = pynibs.load_subject(cls.subject_fn)
        cls.mesh_folder = cls.subject.mesh[cls.mesh_id]['mesh_folder']
        cls.res_folder_e = os.path.join(f"{cls.subject.subject_folder}", "results", f"exp_{cls.exp_id}",
                                        "electric_field",
                                        f"mesh_{cls.mesh_id}", f"roi_{cls.roi_id}")
        cls.res_folder_r = os.path.join(f"{cls.subject.subject_folder}", "results", f"exp_{cls.exp_id}", "r2",
                                        f"mesh_{cls.mesh_id}", f"roi_{cls.roi_id}")
        # remove stuff from old runs
        results_folder = os.path.join(f"{cls.subject.subject_folder}",
                                      "results", f"exp_{cls.exp_id}", "r2", f"mesh_{cls.mesh_id}")

        # delete old files
        if os.path.exists(results_folder):
            shutil.rmtree(results_folder)
            print(f"Removed {results_folder}")

        for fn in ["e.hdf5",
                   "e_scaled.hdf5",
                   "FEM_config.mat"]:
            assert os.path.exists(
                    os.path.join(cls.res_folder_e, fn)), f"{os.path.join(cls.res_folder_e, fn)} wasn't found."

        for e_fn in ["e.hdf5", "e_scaled.hdf5"]:
            fn = os.path.join(cls.res_folder_e, e_fn)
            with h5py.File(fn, 'r') as e:
                for k in ['E', 'E_mag', 'E_norm', 'E_tan']:
                    assert k in e, f"Dataset {k} missing in {fn}"
                    assert np.sum(e[k] == 0) == 0

        print("Test preparation done.")

    def test_01_calc_r_single_core(self):
        """
        Calls our 06_calc_e script with one CPU.
        """
        cmd = f"{EXEC} " \
              f"{self.calc_r_script} " \
              f"-s {self.subject_fn} " \
              f"-m {self.mesh_id} " \
              f"-e {self.exp_id} " \
              f"-r {self.roi_id} " \
              f"-n 1 " \
              f"-f linear " \
              f"-i 0 " \
              f"--convergence"
        print(f"Running {cmd}")
        assert os.system(cmd) == 0, f"Error running {cmd}"

        for muscle in self.muscles:
            for fn in ["r2_roi_data.hdf5", "r2_roi_data.xdmf", "r2_roi_geo.hdf5"]:
                f = os.path.join(self.res_folder_r, muscle, 'linear', fn)
                assert os.path.exists(f), f"{f} wasn't created."

    def test_02_calc_r_multi_core(self):
        """
        Calls our 06_calc_e script with two CPUs
        """
        cmd = f"{EXEC} " \
              f"{self.calc_r_script} " \
              f"-s {self.subject_fn} " \
              f"-m {self.mesh_id} " \
              f"-e {self.exp_id} " \
              f"-r {self.roi_id} " \
              f"-n 2 " \
              f"-f linear " \
              f"-i 0 " \
              f"--convergence"
        print(f"Running {cmd}")
        assert os.system(cmd) == 0, f"Error running {cmd}"

        for muscle in self.muscles:
            for fn in ["r2_roi_data.hdf5", "r2_roi_data.xdmf", "r2_roi_geo.hdf5"]:
                f = os.path.join(self.res_folder_r, muscle, 'linear', fn)
                assert os.path.exists(f), f"{f} wasn't created."


if __name__ == '__main__':
    unittest.main()
