"""Some tests for general import issues of simnibs, pynibs, etc. """
import unittest
import glob
import os
import sys
import platform
from . import SCRIPTS_FOLDER


class Test01CreateSubjectStructure(unittest.TestCase):
    def test_01_sys(self):
        print(f"\nPython v{sys.version} running on {platform.system()}, {platform.release()}, {platform.version()}.")
        print(f"Python exec: {sys.executable}")

    def test_02_pynibs(self):
        import pynibs
        print(f"\nFound pyNIBS v{pynibs.__version__} at {pynibs.__file__}.")

    def test_03_simnibs(self):
        import simnibs
        print(f"\nFound SimNIBS v{simnibs.__version__} at {simnibs.__file__}.")

    def test_04_scripts(self):
        assert os.path.exists(SCRIPTS_FOLDER)
        assert os.path.exists(os.path.join(SCRIPTS_FOLDER, "01_create_subject_structure.py"))
        script_fns = glob.glob(os.path.join(SCRIPTS_FOLDER, '*.py'))
        script_fns.sort()
        print(f'\n{len(script_fns)} TMS Loc scripts found:')
        for fn in script_fns:
            print(f"\t{fn}")

        with open(os.path.join(SCRIPTS_FOLDER, '..', '.git', 'HEAD'), 'r') as f:
            print(f"Branch: {f.readline()}")


if __name__ == '__main__':
    unittest.main()
