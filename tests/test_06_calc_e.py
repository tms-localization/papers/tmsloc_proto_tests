"""
Tests the TMSLoc_Proto script 06_calc_e.py with mesh `06_calc_e` from the `subjectX` subject.
"""
import shutil
import unittest
import os
import pynibs
import h5py
import numpy as np
from . import SCRIPTS_FOLDER, DATA_FOLDER, EXEC


class Test06CalcE(unittest.TestCase):
    # Set path to script repo
    subject = None
    calc_e_script = os.path.join(SCRIPTS_FOLDER, '06_calc_e.py')
    subject_id = 'subjectX'
    subject_fn = os.path.join(DATA_FOLDER, subject_id, f'{subject_id}.hdf5')
    mesh_id = '06_calc_e'
    exp_id = 'reg_isi_05'
    roi_id = 'midlayer_m1s1pmd'

    @classmethod
    def setUpClass(cls):
        """
        Prepare some stuff, call prep_folders.py script to remove any old data that we don't want.
        """
        print("Preparing Test06CalcE.")
        cls.subject = pynibs.load_subject(cls.subject_fn)
        cls.mesh_folder = cls.subject.mesh[cls.mesh_id]['mesh_folder']
        cls.res_folder = os.path.join(f"{cls.subject.subject_folder}", "results", f"exp_{cls.exp_id}", "electric_field",
                                      f"mesh_{cls.mesh_id}", f"roi_{cls.roi_id}")
        # remove stuff from old runs
        results_folder = f"{cls.subject.subject_folder}/results/exp_{cls.exp_id}/electric_field/mesh_{cls.mesh_id}/"
        if os.path.exists(results_folder):
            shutil.rmtree(results_folder)
            print(f"Removed {results_folder}")

        print("Test preparation done.")

    def test_01_calc_e_single_core(self):
        """
        Calls our 06_calc_e script with one CPU

        """
        cmd = f"{EXEC} " \
              f"{self.calc_e_script} " \
              f"-s {self.subject_fn} " \
              f"-m {self.mesh_id} " \
              f"-e {self.exp_id} " \
              f"-r {self.roi_id} " \
              f"-a scalar " \
              f"-n 1"
        print(f"Running {cmd}")
        assert os.system(cmd) == 0, f"Error running {cmd}"

        for fn in ["e.hdf5",
                   "e_scaled.hdf5",
                   "FEM_config.mat"]:
            assert os.path.exists(os.path.join(self.res_folder, fn)), f"{fn} wasn't created."

        for e_fn in ["e.hdf5", "e_scaled.hdf5"]:
            fn = os.path.join(self.res_folder, e_fn)
            with h5py.File(fn, 'r') as e:
                for k in ['E', 'E_mag', 'E_norm', 'E_tan']:
                    assert k in e, f"Dataset {k} missing in {fn}"
                    assert np.sum(e[k] == 0) == 0

    def test_01_calc_e_continue(self):
        """
        Field computations from previous test should be continued.

        """
        cmd = f"{EXEC} " \
              f"{self.calc_e_script} " \
              f"-s {self.subject_fn} " \
              f"-m {self.mesh_id} " \
              f"-e {self.exp_id} " \
              f"-r {self.roi_id} " \
              f"-a scalar " \
              f"-n 1"
        print(f"Running {cmd}")
        assert os.system(cmd) == 0, f"Error running {cmd}"

        for fn in ["e.hdf5",
                   "e_scaled.hdf5",
                   "FEM_config.mat"]:
            assert os.path.exists(os.path.join(self.res_folder, fn)), f"{fn} wasn't created."

        for e_fn in ["e.hdf5", "e_scaled.hdf5"]:
            fn = os.path.join(self.res_folder, e_fn)
            with h5py.File(fn, 'r') as e:
                for k in ['E', 'E_mag', 'E_norm', 'E_tan']:
                    assert k in e, f"Dataset {k} missing in {fn}"
                    assert np.sum(e[k] == 0) == 0

    def test_03_calc_e_multi_core(self):
        """
        Calls our 06_calc_e script with two CPUs
        """
        # remove old FEMs
        self.setUpClass()
        cmd = f"python " \
              f"{self.calc_e_script} " \
              f"-s {self.subject_fn} " \
              f"-m {self.mesh_id} " \
              f"-e {self.exp_id} " \
              f"-r {self.roi_id} " \
              f"-a scalar " \
              f"-n 2"
        print(f"Running {cmd}")
        assert os.system(cmd) == 0, f"Error running {cmd}"

        for fn in ["e.hdf5",
                   "e_scaled.hdf5",
                   "FEM_config.mat"]:
            assert os.path.exists(os.path.join(self.res_folder, fn)), f"{fn} wasn't created."
        for e_fn in ["e.hdf5", "e_scaled.hdf5"]:
            fn = os.path.join(self.res_folder, e_fn)
            with h5py.File(fn, 'r') as e:
                for k in ['E', 'E_mag', 'E_norm', 'E_tan']:
                    assert k in e, f"Dataset {k} missing in {fn}"
                    assert np.sum(e[k] == 0) == 0


if __name__ == '__main__':
    unittest.main()
