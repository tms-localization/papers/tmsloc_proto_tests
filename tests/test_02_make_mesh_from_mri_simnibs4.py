"""
Tests the TMSLoc_Proto script 02_make_msh_from_mri_simnibs4.py with mesh `02_make_mesh_from_mri_simnibs4` from
the `spheremodel` subject.
"""
import glob
import unittest
import os
import pynibs
import nibabel as nib
import numpy as np
from . import TEST_FOLDER, SCRIPTS_FOLDER, DATA_FOLDER, EXEC
from simnibs import __version__ as vsimnibs


class Test02MakeMeshFromMriSimnibs4(unittest.TestCase):
    # Set path to script repo
    charm_mesh_script_fn = os.path.join(SCRIPTS_FOLDER,
                                        '02_make_msh_from_mri_simnibs4.py')
    subject_id = 'spheremodel'
    subject_fn = os.path.join(DATA_FOLDER, subject_id, f"{subject_id}.hdf5")
    charm_ini_fn = os.path.join(TEST_FOLDER, '..', 'config', 'charm.ini')
    mesh_id = '02_make_mesh_from_mri_simnibs4'

    subject = None
    mesh_folder = None

    @classmethod
    def setUpClass(cls):
        """
        Prepare some stuff, call prep_folders.py script to remove any old data that we don't want.
        """
        print("Preparing Test03Test02MakeMeshFromMriSimnibs4.")
        cls.subject = pynibs.load_subject(cls.subject_fn)
        cls.mesh_folder = cls.subject.mesh[cls.mesh_id]['mesh_folder']

        # remove stuff from old runs
        # delete old .hdf5 and .msh files
        for fn in glob.glob(f"{cls.mesh_folder}/m2m_{cls.subject.id}/{cls.subject.id}*.hdf5"):
            os.unlink(fn)
            print(f"Removed {fn}")
        for fn in glob.glob(f"{cls.mesh_folder}/m2m_{cls.subject.id}/{cls.subject.id}*.msh"):
            os.unlink(fn)
            print(f"Removed {fn}")
        for fn in glob.glob(f"{cls.mesh_folder}/m2m_{cls.subject.id}/{cls.subject.id}*.xdmf"):
            os.unlink(fn)
            print(f"Removed {fn}")

        # move T1.nii to final_tissues
        t1_fn = cls.subject.mri[cls.subject.mesh[cls.mesh_id]['mri_idx']]['fn_mri_T1']
        labeling_fn = f"{cls.mesh_folder}/m2m_{cls.subject.id}/label_prep/tissue_labeling_upsampled.nii.gz"
        if os.path.exists(labeling_fn):
            os.unlink(labeling_fn)
            print(f"Removed {labeling_fn}")

        print(f"Replacing label image {labeling_fn} with T1 image {t1_fn}.")
        labeling_img = nib.load(t1_fn)
        labeling_img.to_filename(labeling_fn)

        print("Test preparation done.")

    def test_01_mesh_prep(self):
        """
        Checks if the folder/file preparatios are ok.

        """
        if vsimnibs.startswith('3'):
            print(f"SimNIBS {vsimnibs} found. Skipping test.")
        else:
            assert os.path.exists(f"{self.mesh_folder}/m2m_{self.subject.id}/"), "Mesh folder structure does not exist"
            t1_fn = self.subject.mri[self.subject.mesh[self.mesh_id]['mri_idx']]['fn_mri_T1']
            t1 = nib.load(t1_fn)
            labeling_fn = f"{self.mesh_folder}/m2m_{self.subject.id}/label_prep/tissue_labeling_upsampled.nii.gz"
            labeling = nib.load(labeling_fn)

            assert np.all(t1.get_fdata() == labeling.get_fdata()), f"Labelling img {labeling_fn} has not been" \
                                                                   f"replaced by the T1 image ({t1_fn})"
            msh_fn = f"{self.mesh_folder}/m2m_{self.subject.id}/{self.subject.id}.msh"
            assert not os.path.exists(msh_fn), \
                f"Old .msh file {msh_fn} has not been removed."

    def test_02_mri2meshcharm(self):
        """
        Calls our mri2msh script, creates ROIs and mesh.hdf5 files.

        """
        if vsimnibs.startswith('3'):
            print(f"SimNIBS {vsimnibs} found. Skipping test.")
        else:
            cmd = f"{EXEC} {self.charm_mesh_script_fn} " \
                  f"-s {self.subject_fn} " \
                  f"-m {self.mesh_id} " \
                  f"--charm_ini={self.charm_ini_fn} " \
                  f"--charm_cmd_params=mesh"
            print(f"Running {cmd}")
            os.system(cmd)

            for roi_id, roi in self.subject.roi[self.mesh_id].items():
                assert os.path.exists(f"{self.mesh_folder}/{roi['fn_mask']}"), "fn_mask missing in roi folder"
                assert os.path.exists(f"{self.mesh_folder}/roi/{roi_id}/geo.xdmf"), 'xdmf ROI file is missing'

            for fn in [f"{self.mesh_folder}/m2m_{self.subject.id}/{self.subject.id}.msh",
                       f"{self.mesh_folder}/m2m_{self.subject.id}/{self.subject.id}.xdmf",
                       f"{self.mesh_folder}/m2m_{self.subject.id}/{self.subject.id}_org.xdmf",
                       f"{self.mesh_folder}/m2m_{self.subject.id}/{self.subject.id}_smoothed.xdmf"]:
                assert os.path.exists(fn), f"{fn} is missing."


if __name__ == '__main__':
    unittest.main()
