"""
Tests the TMSLoc_Proto script 09_estimate_rmt_from_data.py with mesh `08_calc_opt_coil_pos.py`
from the `subjectX` subject.
"""
import shutil
import unittest
import os
import pynibs
import h5py
import numpy as np
from . import SCRIPTS_FOLDER, DATA_FOLDER


class Test09EstimateRmtFromData(unittest.TestCase):
    test_folder = os.path.dirname(os.path.abspath(__file__))
    est_rmt_fn = os.path.join(SCRIPTS_FOLDER, "09_estimate_rmt_from_data.py")
    subject_id = 'subjectX'
    subject_fn = os.path.join(DATA_FOLDER, subject_id, f'{subject_id}.hdf5')
    mesh_id = '09_estimate_rmt_from_data'
    exp_id = 'reg_isi_05'
    roi_id = 'midlayer_m1s1pmd'
    muscle = 'FDI'
    funtype = 'linear'

    # is set in setUpClass() at runtime
    res_folder_opt = None
    subject = None
    fn_gof = None
    fn_e = None
    fn_exp = None
    fn_e_opt = None

    @classmethod
    def setUpClass(cls):
        """
        Prepare some stuff.
        """
        print("Preparing Test09EstimateRmtFromData.")
        cls.subject = pynibs.load_subject(cls.subject_fn)
        cls.fn_gof = os.path.join(cls.subject.subject_folder,
                                  'results',
                                  f'exp_{cls.exp_id}',
                                  'r2',
                                  f'mesh_{cls.mesh_id}',
                                  f'roi_{cls.roi_id}',
                                  cls.muscle,
                                  cls.funtype,
                                  'r2_roi_data.hdf5')

        cls.fn_e = os.path.join(cls.subject.subject_folder,
                                'results',
                                f'exp_{cls.exp_id}',
                                'electric_field',
                                f'mesh_{cls.mesh_id}',
                                f'roi_{cls.roi_id}',
                                'e_scaled.hdf5')

        cls.fn_exp = os.path.join(cls.subject.subject_folder,
                                  'exp',
                                  cls.exp_id,
                                  f'mesh_{cls.mesh_id}',
                                  'experiment.hdf5')

        cls.fn_e_opt =os.path.join(cls.subject.subject_folder,
                                   'opt',
                                   f'exp_{cls.exp_id}',
                                   'target_[+10.00 +20.00 +30.00]',
                                   f'mesh_{cls.mesh_id}',
                                   'E_mag',
                                   'coil_positions.geo')
        print("Test preparation done.")

    def test_01_estimate_rmt(self):
        """

        """
        cmd = f"python " \
              f"{self.est_rmt_fn} " \
              f"--fn_gof={self.fn_gof} " \
              f"--fn_e={self.fn_e} " \
              f"--fn_exp={self.fn_exp} " \
              f"--fn_e_opt='{self.fn_e_opt}' " \
              f"--muscle={self.muscle} " \
              f"--fun_type={self.funtype} "
        print(f"Running {cmd}")
        assert os.system(cmd) == 0, f"Error running {cmd}"

        opt_folder = os.path.split(self.fn_e_opt)[0]
        assert os.path.exists(os.path.join(opt_folder, 'fit_in_hotspot_rmt_estimation.png')), f"{f} wasn't created."


if __name__ == '__main__':
    unittest.main()
