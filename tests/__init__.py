"""
Here, all test files to test the TMSLoc_proto repository are stored.

    Example
    -------
    .. code-block:: sh

        cd tmsloc_proto_tests
        python tests

    or

    .. code-block:: sh

        cd tmsloc_proto_tests
        coverage run -m pytest -s tests

This __init__.py file sets up some repo-wide path variables.

"""
import os
import sys

# Set path to script repo
TEST_FOLDER = os.path.dirname(os.path.abspath(__file__))
SCRIPTS_FOLDER = os.path.join(TEST_FOLDER, '..', '..', 'tmsloc_proto', 'scripts')
DATA_FOLDER = os.path.join(TEST_FOLDER, '..', 'data')

# if available use 'coverage' to run scripts to collect code coverage for the TMSLoc_proto repo.
if os.system('coverage') == 0:
    EXEC = 'coverage run -a'
else:
    EXEC = sys.executable
