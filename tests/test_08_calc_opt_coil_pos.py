"""
Tests the TMSLoc_Proto script 08_calc_opt_coil_pos.py with mesh `08_calc_opt_coil_pos` from the `subjectX` subject.
"""
import os
import shutil
import pynibs
import unittest
import numpy as np
from . import SCRIPTS_FOLDER, DATA_FOLDER, EXEC


class Test08CalcOptCoilPois(unittest.TestCase):
    test_folder = os.path.dirname(os.path.abspath(__file__))
    calc_opt_coil_script = os.path.join(SCRIPTS_FOLDER, "08_calc_opt_coil_pos.py")
    subject_id = 'subjectX'
    subject_fn = os.path.join(DATA_FOLDER, subject_id, f'{subject_id}.hdf5')
    mesh_id = '08_calc_opt_coil_pos'
    exp_id = 'reg_isi_05'
    roi_id = 'midlayer_m1s1pmd'
    target = np.array((10., 20., 30.))
    coil_name = "Medtronic_MCF_B65_REF.nii.gz"

    # is set in setUpClass() at runtime
    res_folder_opt = None
    coil_fn = None
    subject = None
    optim_dir = None

    opt_search_radius = 20
    opt_search_angle = 1
    opt_angle_resolution = 180
    opt_spatial_resolution = 10
    opt_smooth = 100
    opt_distance = 1

    @classmethod
    def setUpClass(cls):
        """
        Prepare some stuff.
        """
        print("Preparing Test08CalcOptCoilPois.")
        cls.subject = pynibs.load_subject(cls.subject_fn)
        cls.mesh_folder = cls.subject.mesh[cls.mesh_id]['mesh_folder']
        cls.results_folder_opt = os.path.join(f"{cls.subject.subject_folder}", "results", f"exp_{cls.exp_id}", "r2")
        cls.optim_dir = os.path.join(cls.subject.subject_folder,
                                     f"opt",
                                     f"exp_{cls.exp_id}",
                                     f"target_"
                                     f"{np.array2string(cls.target, formatter={'float_kind': '{0:+0.2f}'.format})}",
                                     f"mesh_{cls.mesh_id}",
                                     f"E_mag")
        cls.coil_fn = os.path.join(DATA_FOLDER, 'coils', cls.coil_name)
        try:
            shutil.rmtree(cls.optim_dir)
            print(f"Removed {cls.optim_dir}")
        except OSError:
            pass

        print("Test preparation done.")

    def test_01_calc_opt_coil_single_core(self):
        """
        Calls our 06_calc_e script with one CPUs
        """
        cmd = f"{EXEC} " \
              f"{self.calc_opt_coil_script} " \
              f"-s {self.subject_fn} " \
              f"-m {self.mesh_id} " \
              f"-e {self.exp_id} " \
              f"-n 1 " \
              f"-t {self.target[0]} {self.target[1]} {self.target[2]} " \
              f"--opt_search_radius={self.opt_search_radius} " \
              f"--opt_search_angle={self.opt_search_angle} " \
              f"--opt_angle_resolution={self.opt_angle_resolution} " \
              f"--opt_spatial_resolution={self.opt_spatial_resolution} " \
              f"--opt_smooth={self.opt_smooth} " \
              f"--opt_distance={self.opt_distance} " \
              f"--fn_coil={self.coil_fn} " \
              f"--anisotropy_type=scalar"
        print(f"Running {cmd}")
        assert os.system(cmd) == 0, f"Error running {cmd}"

        for fn in ["search_positions.xdmf",
                   "opt_coil_pos.xml",
                   f"{self.subject_id}_TMS_optimize_{self.coil_name}_nii.msh", "target.msh", "coil_positions.geo",
                   os.path.join("e_opt",
                                f"{self.subject_id}_1-0001_TMS_optimize_{self.coil_name.replace('.nii.gz', '')}"
                                f"_nii_scalar.msh"),
                   os.path.join("e_opt", "subject_overlays", "rh.central"),
                   os.path.join("e_opt", "subject_overlays", "lh.central"),
                   os.path.join("e_opt", "e.xdmf")]:
            f = os.path.join(self.optim_dir, "e_opt", "subject_overlays",
                             f"{self.subject_id}_TMS_1-0001_"
                             f"{self.coil_name.replace('.nii.gz', '')}_nii_scalar_central.msh")
            assert os.path.exists(f), f"{f} wasn't created."

    def test_02_continue_existing_optim(self):
        """ When an existing optimation is found for a given target, only the final FEM should run."""
        self.test_01_calc_opt_coil_single_core()


if __name__ == '__main__':
    unittest.main()
