"""
Tests the TMSLoc_Proto script 04_get_initial_m1_coords.py with mesh `05_merge_exp_data` from
the `spheremodel` subject.
"""
import unittest
import os
import pynibs
from . import SCRIPTS_FOLDER, DATA_FOLDER, EXEC


class Test04GetInitialM1Coords(unittest.TestCase):
    # Set path to script repo
    subject = None
    get_m1_coords = os.path.join(SCRIPTS_FOLDER, '04_get_initial_m1_coords.py')
    subject_id = 'spheremodel'
    subject_fn = os.path.join(DATA_FOLDER, subject_id, f'{subject_id}.hdf5')
    mesh_id = '02_make_mesh_from_mri_simnibs4'

    @classmethod
    def setUpClass(cls):
        print("Preparing Test04GetInitialM1Coords.")
        cls.subject = pynibs.load_subject(cls.subject_fn)
        cls.mesh_folder = cls.subject.mesh[cls.mesh_id]['mesh_folder']
        print("Test preparation done.")

    def test_01_get_initial_m1_coords(self):
        """
        Call 04_get_initial_m1_coords
        """
        assert os.system(f"{EXEC} {self.get_m1_coords} "
                         f"-s {self.subject_fn} -m {self.mesh_id}") == 0


if __name__ == '__main__':
    unittest.main()
